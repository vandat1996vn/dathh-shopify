







var GUI = (function () {
  var win = $(window);
  var html = $("html,body");

  var faqToggle = function () {
    $(".items-faq__mains:first-child").find(".answer-faq__items").slideDown();

    $(".btn-faq__mains").click(function(){
      $(this).parents(".items-faq__mains").find(".answer-faq__items").slideToggle();
      $(".btn-faq__mains").not(this).parents(".items-faq__mains").find(".answer-faq__items").slideUp();
    });    
  };

  return {
    _: function () {
      faqToggle();
    }
  };
})();

$(document).ready(function () {
  // if (/Lighthouse/.test(navigator.userAgent)) {
  //     return;
  // }
  GUI._();
});